/* Задача № 1. Вычисляем максимальное из двух чисел (Рубанов В.В. QA 4-2018)*/

package org.itstep.mathematic.operations;
import java.util.Scanner;

public class MinMax {

    public static void main(String[] args) {

        Scanner inn = new Scanner(System.in);
        System.out.println("Введите целое число a:");
        int a = inn.nextInt();

        System.out.println("Введите целое число b:");
        int b = inn.nextInt();

        int maxValue = 0;

        if (a>b) {
            maxValue = a;
            System.out.println("MAX(a,b) = " + maxValue);
        }
        else {
            if (a<b){
                maxValue = b;
                System.out.println("MAX(a,b) = " + maxValue);
            }
            else
                System.out.println("Значения переменных a и b равны между собой " + a);

        }

        }

    }

